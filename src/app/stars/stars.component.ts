import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-stars',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.scss']
})
export class StarsComponent implements OnInit {
  @Input()
  private rating : number = 0
  private star : Array<boolean>
  constructor() { }
  ngOnInit() {
    this.star = [true,true,true,true,true]
  }

}
