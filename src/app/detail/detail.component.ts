import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  detailId: number
  constructor(private routeInfo: ActivatedRoute) { }

  ngOnInit() {
    this.detailId = this.routeInfo.snapshot.queryParams.id
  }

}
