import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  private products: Array<Product> 
  constructor(private routeInfo: ActivatedRoute) { }

  ngOnInit() {
    this.products = [
      new Product('1', 200, ' bbbcfgdfgdf')
    ]
    console.log(this.routeInfo.snapshot.queryParams['id'])
  }
}
export class Product {
  constructor(
    id: string,
    price: number,
    desc: string
  ) {}
}
